SELECT  amphurs.amphur_id , amphurs.amphur_name_eng, districts.district_name_eng, COUNT(district_name_eng) 
from amphurs
inner join districts on amphurs.amphur_id = districts.amphur_id
GROUP BY district_name_eng
HAVING COUNT(district_name_eng) > 1