<?php    
    function get_data(){
        $connect = mysqli_connect("localhost", "root", "", "thailand");

        $query_provinces = 'SELECT * 
    FROM provinces';

        $query_amphurs = 'SELECT *
    FROM amphurs';

        $query_districts = 'SELECT *
    FROM districts';
        mysqli_query($connect, "SET CHARACTER SET 'utf8'");
        mysqli_query($connect, "SET SESSION collation_connection ='utf8_general_ci'");

        $result_provinces = mysqli_query($connect, $query_provinces);
        $result_amphurs = mysqli_query($connect, $query_amphurs);
        $result_districts = mysqli_query($connect, $query_districts);
        $thailand = array();

        $key = 0;
        foreach ($result_provinces as $key => $provinces) {
            $key += 1;
            $thailand[$key]['PROVINCE_ID'] = (int)($provinces['province_id']);
            $thailand[$key]['PROVINCE_CODE'] = $provinces['province_code'];
            $thailand[$key]['PROVINCE_NAME'] = $provinces['province_name'];
            $thailand[$key]['PROVINCE_NAME_ENG'] = $provinces['province_name_eng'];
            $thailand[$key]['GEO_ID'] = (int)($provinces['geo_id']);
            
            $query_amphurs = 'SELECT * FROM amphurs WHERE province_id='.$provinces['province_id'].' ';
            $result_amphurs = mysqli_query($connect, $query_amphurs);

            $key2 = 0;
            foreach ($result_amphurs as $key2 => $amphurs) {
                $key2 += 1;
                $key2 = $amphurs['amphur_id'];
                $thailand[$key]['amphurs'][$key2]['AMPHUR_ID'] = (int)($amphurs['amphur_id']);
                $thailand[$key]['amphurs'][$key2]['AMPHUR_CODE'] = $amphurs['amphur_code'];
                $thailand[$key]['amphurs'][$key2]['AMPHUR_NAME'] = $amphurs['amphur_name'];
                $thailand[$key]['amphurs'][$key2]['AMPHUR_NAME_ENG'] = $amphurs['amphur_name_eng'];
                $thailand[$key]['amphurs'][$key2]['GEO_ID'] = (int)($amphurs['geo_id']);
                $thailand[$key]['amphurs'][$key2]['PROVINCE_ID'] = (int)($amphurs['province_id']);

                $query_districts = 'SELECT * FROM districts WHERE amphur_id='.$amphurs['amphur_id'].' ';
                $result_districts = mysqli_query($connect, $query_districts);

                $key3 = 0;
                foreach ($result_districts as $key3 => $districts) {

                    $key3 += 1;
                    $key3 = $districts['district_id'];
                    $thailand[$key]['amphurs'][$key2]['districts'][$key3]['DISTRICT_ID'] = (int)($districts['district_id']);
                    $thailand[$key]['amphurs'][$key2]['districts'][$key3]['DISTRICT_CODE'] = $districts['district_code'];
                    $thailand[$key]['amphurs'][$key2]['districts'][$key3]['DISTRICT_NAME'] = $districts['district_name'];
                    $thailand[$key]['amphurs'][$key2]['districts'][$key3]['DISTRICT_NAME_ENG'] = $districts['district_name_eng'];
                    $thailand[$key]['amphurs'][$key2]['districts'][$key3]['AMPHUR_ID'] = (int)($districts['amphur_id']);
                    $thailand[$key]['amphurs'][$key2]['districts'][$key3]['PROVINCE_ID'] = (int)($districts['province_id']);
                    $thailand[$key]['amphurs'][$key2]['districts'][$key3]['GEO_ID'] = (int)($districts['geo_id']);
                }
            }
        }

        return ($object = json_encode($thailand, JSON_FORCE_OBJECT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE));
    }

    $file_name = 'ThailandLocation2' . '.json';

    if (file_put_contents($file_name, get_data())) {
        echo $file_name . ' file created';
    } else {
        echo 'Some error';
    }


?>